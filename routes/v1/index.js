const manga = require("./manga.js");
const manhua = require("./manhua.js");
const manhwa = require("./manhwa.js");
const search = require("./search.js");

const { Router } = require("express");
const router = Router();

router.use("/komik", manga);
router.use("/komik", manhua);
router.use("/komik", manhwa);
router.use("/komik", search);

module.exports = router;
