const express = require("express");
const Authenticated = require("../../middleware/Authenticated.js");
const router = express.Router();
const {
  getAllManhua,
  getManhuaChapter,
} = require("../../controllers/v1/manhua.js");

router.get("/manhua", Authenticated, getAllManhua);
router.get("/chapter/:chapter_id", Authenticated, getManhuaChapter);
module.exports = router;
