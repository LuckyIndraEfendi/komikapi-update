const express = require("express");
const Authenticated = require("../../middleware/Authenticated.js");
const router = express.Router();
const {
  getAllManhwa,
  getManhwaChapter,
} = require("../../controllers/v1/manhwa.js");
router.get("/manhwa", Authenticated, getAllManhwa);
router.get("/chapter/:chapter_id", Authenticated, getManhwaChapter);
module.exports = router;
