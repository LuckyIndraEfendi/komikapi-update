const express = require("express");
const Authenticated = require("../../middleware/Authenticated.js");
const router = express.Router();
const {
  getAllManga,
  getMangaDetails,
  getMangaChapter,
  getPopularManga,
} = require("../../controllers/v1/manga.js");
router.get("/manga", Authenticated, getAllManga);
router.get("/manga/popular", Authenticated, getPopularManga);
router.get("/details/:details_id", Authenticated, getMangaDetails);
router.get("/chapter/:chapter_id", Authenticated, getMangaChapter);
module.exports = router;
