const express = require("express");
const Authenticated = require("../../middleware/Authenticated.js");
const router = express.Router();
const { searchAll } = require("../../controllers/v1/searchAll.js");
router.get("/search", Authenticated, searchAll);

module.exports = router;
