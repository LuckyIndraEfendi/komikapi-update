const { gotScraping } = require("got-scraping");
const cheerio = require("cheerio");
const userAgent = require("../../helpers/userAgent.js");
const dotenv = require("dotenv");
const { baseURL } = require("../../helpers/apiKey.js");
dotenv.config();
const getAllManhua = async (req, res) => {
  try {
    const response = await gotScraping.get(
      `${baseURL}/baca-manhua/page/${req.query.page || 1}`,
      {
        headers: userAgent,
      }
    );
    const html = response.body;

    const $ = cheerio.load(html);
    const manga = [];
    const hasPrev = $(
      "#content > div.postbody > section.whites > div.widget-body > div > div > div.pagination > a.prev.page-numbers"
    )
      .text()
      .replace("«", "");
    const hasNext = $(
      "#content > div.postbody > section.whites > div.widget-body > div > div > div.pagination > a.next.page-numbers"
    )
      .text()
      .replace("Berikutnya ", "");
    $(
      "#content > div.postbody > section.whites > div.widget-body > div > div > div.listupd > div"
    ).each((i, el) => {
      manga.push({
        title: $(el).find("div > a > div > h4").text(),
        image: $(el)
          .find("div > a > div > img")
          ?.attr("data-lazy-src")
          ?.replace(/\?width=\d+&height=\d+/g, "")
          ?.replace(/\?resize=\d+,\d+/g, ""),
        release_time: $(el)
          .find("div > div > div > div > span")
          .text()
          ?.replace(/\s+/g, " "),
        details_id: `/details/${$(el)
          .find(" div > a")
          .attr("href")
          ?.replace(`${baseURL}/komik`, "")
          ?.replace("/", "")
          ?.replace("/", "")}`,
      });
    });

    res.json({
      statusCode: 200,
      message: "Get All Manga",
      data: manga,
      total_items: manga.length,
      has_next: !!hasNext,
      has_prev: !!hasPrev,
    });
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

const getManhuaChapter = async (req, res) => {
  try {
    const response = await gotScraping.get(
      `${baseURL}/${req.params.chapter_id}/`,
      { headers: userAgent }
    );

    const html = response.body;

    const $ = cheerio.load(html);

    let chapter = [];
    $("#anjay_ini_id_kh > img").each((i, el) => {
      chapter.push({
        chapter_image: $(el).attr("data-lazy-src"),
        chaper_imagetitle: $(el).attr("alt"),
      });
    });

    const has_prev = $("div.nextprev > a[rel='prev']").length > 0;
    const has_next = $("div.nextprev > a[rel='next']").length > 0;
    res.send({
      statusCode: 200,
      data: {
        chapter: {
          chapter: chapter,
          total_items: chapter.length,
        },
        has_prev,
        has_next,
      },
    });
  } catch (err) {
    res.status(500).json({
      statusCode: 500,
      message: err.message,
    });
  }
};

module.exports = { getAllManhua, getManhuaChapter };
