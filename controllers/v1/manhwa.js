const { gotScraping } = require("got-scraping");
const cheerio = require("cheerio");
const userAgent = require("../../helpers/userAgent.js");
const dotenv = require("dotenv");
const { baseURL } = require("../../helpers/apiKey.js");

dotenv.config();
const getAllManhwa = async (req, res) => {
  try {
    const response = await gotScraping.get(
      `${baseURL}/baca-manhwa/page/${req.query.page || 1}`,
      {
        headers: userAgent,
      }
    );
    const html = response.body;

    const $ = cheerio.load(html);
    const manga = [];
    const hasPrev = $(
      "#content > div.postbody > section.whites > div.widget-body > div > div > div.pagination > a.prev.page-numbers"
    )
      .text()
      .replace("«", "");
    const hasNext = $(
      "#content > div.postbody > section.whites > div.widget-body > div > div > div.pagination > a.next.page-numbers"
    )
      .text()
      .replace("Berikutnya ", "");
    $(
      "#content > div.postbody > section.whites > div.widget-body > div > div > div.listupd > div"
    ).each((i, el) => {
      manga.push({
        title: $(el).find("div > a > div > h4").text(),
        image: $(el)
          .find("div > a > div > img")
          ?.attr("data-lazy-src")
          ?.replace(/\?width=\d+&height=\d+/g, "")
          ?.replace(/\?resize=\d+,\d+/g, ""),
        release_time: $(el)
          .find("div > div > div > div > span")
          .text()
          ?.replace(/\s+/g, " "),
        details_id: `/details/${$(el)
          .find(" div > a")
          .attr("href")
          ?.replace(`${baseURL}/komik`, "")
          ?.replace("/", "")
          ?.replace("/", "")}`,
      });
    });

    res.json({
      statusCode: 200,
      message: "Get All Manga",
      data: manga,
      total_items: manga.length,
      has_next: !!hasNext,
      has_prev: !!hasPrev,
    });
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

const getmanhwaDetails = async (req, res) => {
  try {
    const response = await gotScraping.get(
      `${baseURL}/komik/${req.params.details_id}/`,
      { headers: userAgent }
    );

    const html = response.body;

    const $ = cheerio.load(html);

    const images = $(
      "#post-168941 > section:nth-child(1) > div > div.thumb > img"
    )
      .attr("data-lazy-src")
      .replace(/\?resize=\d+,\d+/g, "");
    const status = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.spe > span:nth-child(2)"
    )
      .text()
      .replace("Status:", "")
      .replace(/\s+/g, " ");
    const type = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.spe > span:nth-child(3) > a"
    )
      .text()
      .replace(/\s+/g, " ");
    const pengarang = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.spe > span:nth-child(4) > a"
    )
      .map((i, el) => $(el).text().replace(/\s+/g, " "))
      .get();
    const rilis = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.spe > span:nth-child(6) > a"
    ).text();
    const serialize = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.spe > span:nth-child(7)"
    )
      .text()
      ?.replace(/\s+/g, " ");
    const dilihat = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.spe > span:nth-child(8)"
    )
      .text()
      .replace("Jumlah Pembaca:", "")
      .replace(/\s+/g, " ");
    // let genres = [];
    const genres = $(
      "#post-168941 > section:nth-child(1) > div > div.infox > div.genre-info > a"
    )
      .map((i, el) => $(el).text().replace(/\s+/g, " "))
      .get();
    const synopsis = $(
      "#sinopsis > section > div > div.entry-content.entry-content-single > p"
    )
      .text()
      .replace(/\s+/g, " ");
    const rating = $(
      "#post-168941 > section:nth-child(1) > div > div.thumb > div > div > div > div > i"
    )
      .text()
      .replace(/\s+/g, " ");

    let chapter_list = [];

    $("#chapter_list > ul > li").each((i, el) => {
      chapter_list.push({
        chapter_title: $(el).find("span.lchx > a").text().replace(/\s+/g, " "),
        chapter_link: `/chapter/${$(el)
          .find("span.lchx > a")
          .attr("href")
          .replace(`${baseURL}/`, "")}`,
        chapter_update: $(el).find("span.dt > a").text(),
      });
    });

    let komikLainnya = [];
    $("#mirip > div > div > ul > li").each((i, el) => {
      komikLainnya.push({
        title: $(el).find("div.leftseries > h4 > a").text(),
        images: $(el).find("div.imgseries > a > img").attr("data-lazy-src"),
        details_id: `/details/${$(el)
          .find("div.leftseries > h4 > a")
          .attr("href")
          .replace(`${baseURL}/komik`, "")
          .replace("/", "")}`,
      });
    });

    res.send({
      statusCode: 200,
      data: {
        status,
        images,
        type,
        synopsis,
        rilis,
        pengarang,
        dilihat,
        rating,
        serialize,
        genres,
        chapter_list: {
          chapter_list,
          total_items: chapter_list.length,
        },
        komik_lainnya: {
          komikLainnya,
          total_items: komikLainnya.length,
        },
      },
    });
  } catch (err) {
    res.status(500).json({
      statusCode: 500,
      message: err.message,
    });
  }
};

const getManhwaChapter = async (req, res) => {
  try {
    const response = await gotScraping.get(
      `${baseURL}/${req.params.chapter_id}/`,
      { headers: userAgent }
    );

    const html = response.body;

    const $ = cheerio.load(html);

    let chapter = [];
    $("#anjay_ini_id_kh > img").each((i, el) => {
      chapter.push({
        chapter_image: $(el).attr("data-lazy-src"),
        chaper_imagetitle: $(el).attr("alt"),
      });
    });

    const has_prev = $("div.nextprev > a[rel='prev']").length > 0;
    const has_next = $("div.nextprev > a[rel='next']").length > 0;
    res.send({
      statusCode: 200,
      data: {
        chapter: {
          chapter: chapter,
          total_items: chapter.length,
        },
        has_prev,
        has_next,
      },
    });
  } catch (err) {
    res.status(500).json({
      statusCode: 500,
      message: err.message,
    });
  }
};

module.exports = { getAllManhwa, getmanhwaDetails, getManhwaChapter };
