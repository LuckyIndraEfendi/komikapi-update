FROM node:18-alpine
WORKDIR /komik-api
ENV NODE_ENV=production
ENV DATABASE_URL=
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install
COPY . .
CMD [ "node", "index.js" ]
