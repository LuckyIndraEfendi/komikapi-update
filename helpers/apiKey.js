const dotenv = require("dotenv");
dotenv.config();
const apiKey = `${process.env.API_KEY}`;
const baseURL = `https://bacakomik.net`;
module.exports = { apiKey, baseURL };
